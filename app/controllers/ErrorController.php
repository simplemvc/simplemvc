<?php namespace App\Controllers;

use SimpleMVC\Http\Controller;
use SimpleMVC\Http\Request;

class ErrorController extends Controller
{
    // Place methods here

    public function error404()
    {
        $this->render('errors.404');
    }
}