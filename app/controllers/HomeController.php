<?php namespace App\Controllers;

use SimpleMVC\Http\Controller;
use SimpleMVC\Http\Request;

use SimpleMVC\Session\Session;

use App\Models\User;

class HomeController extends Controller
{
    /**
     * The method loaded from the default home page
     *
     * @return void
     */
	public function index()
	{
        if(\Session::has('data'))
            $this->add('data', \Session::get('data'));
        \Session::delete('data');

        $user = new User;

        $this->render('home.index');
	}

    /**
     * The method called when using site search
     * This renders the home.seach view
     *
     * @param \SimpleMVC\Http\Request $request
     * @return void
     */
    public function search(Request $request)
    {
        if(isset($request->q) && !empty($request->q))
        {
            $data = (object)array('query' => urldecode($request->q), 'category' => $request->cat);
            $this->add('data', $data)->render('home.search');
        }
        else
        {
            $this->flash('msg', 'You forgot to supply something in the search box!')
                ->redirect('/');
        }
    }

    /**
     * The method called when using the search box on the home page
     *
     * @param \SimpleMVC\Http\Request $request
     * @return void
     */
    public function searchHome(Request $request)
    {
        if(isset($request->q) && !empty($request->q))
        {
            $data = array('query' => urldecode($request->q), 'category' => $request->cat);

            $this->redirectWith('/', array('data' => (object)$data));
        }
        else
        {
            $this->flash('msg', 'You forgot to supply something in the search box!')
                ->redirect('/');
        }
    }

    /**
     * The method called when performing a google search
     *
     * @param \SimpleMVC\Http\Request $request
     * @return void
     */
    public function searchGoogle(Request $request)
    {
        if(isset($request->q) && !empty($request->q))
        {
            $data = "?q=" . urlencode($request->q);
            $this->redirect('https://google.com/search' . $data);
        }
        else
        {
            $this->flash('msg', 'You forgot to supply something in the search box!')
                ->redirect('/');
        }
    }

    /**
     * The method called for rendering user creation page
     *
     * @return void
     */
    public function getCreateUser()
    {
        $this->render('home.create');
    }
}
