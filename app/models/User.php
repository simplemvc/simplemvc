<?php namespace App\Models;

use SimpleMVC\Http\Model;

class User extends Model
{
    protected $fillable = array('username', 'password');
}