@extends('layout.main')

@section('content')

{{ Session::flash('msg') }}

{{-- This is for temporary data sent via redirect --}}
@if(isset($data))
	You searched for {{ $data->query }} with category: {{ $data->category }}
@endif

{{ Form::open(array('action' => '/search', 'method' => 'GET')) }}
{{ Form::label('q', 'Search:') }}
{{ Form::text('q') }}
{{ Form::hidden('cat', '5') }}
{{ Form::submit('Search Site', ['class' => 'btn btn-primary']) }}
{{ Form::close() }}

<br>

{{ Form::open(array('action' => '/searchHome', 'method' => 'GET')) }}
{{ Form::label('q', 'Search:') }}
{{ Form::text('q') }}
{{ Form::hidden('cat', '5') }}
{{ Form::submit('Search w/ Flash Message', ['class' => 'btn btn-primary']) }}
{{ Form::close() }}

<br>

{{ Form::open(array('action' => '/searchGoogle', 'method' => 'POST')) }}
{{ Form::label('q', 'Search:') }}
{{ Form::text('q') }}
{{ Form::submit('Search Google', ['class' => 'btn btn-primary']) }}
{{ Form::close() }}

{{ Html::link('/create', 'Create User') }}

@stop