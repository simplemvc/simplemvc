@extends('layout.main')

@section('content')

You searched for: {{ $data->query }} in category: {{ $data->category }}

<br>

{{ Html::link('/', '&laquo; Return Home', array('class' => 'btn btn-primary')) }}

@stop