<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <title>Mr. Landry's Website</title>
    <link rel="stylesheet" type="text/css" href="{{ Html::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ Html::asset('css/theme.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ Html::asset('sss/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ Html::asset('css/site.min.css')}}">
    <script type="text/javascript" src="{{ Html::asset('js/global.min.js') }}"></script>
    <script type="text/javascript">
        /*
         * Landry Website JavaScript
         * (c) 2015 Kaleb Klein | All Rights Reserved
         *
         * Page Loading Scripts
         */
        // $(function(){$("#pageContent").hide();});$(function(){$(".pageContent").fadeIn("slow");var e=window.location,t=e.pathname.split("/");"index.html"===t[0].toLowerCase()||""===t[2]?$.get("pages/index.html",{},function(e){$(".pageContent").html(e)}):"template"===t[2]?$.get("template.php",function(e){$(".pageContent").html(e)}):"course"===t[2]&&(""===t[3]?(alert("The page you are looking for doesn't exist! You will be redirected back to the home page"),window.location="../"):$.get("../pages/course.php",{course:t[3]},function(e){$(".pageContent").html(e)})),$.get("/~rlandry/pages/nav.php",{},function(e){$(".navbar-nav").html(e)}),$(".date").html((new Date).getFullYear())});
    </script>
</head>
<body>
    <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
        <div class="navbar-inner">
            <div class="container">
                <a class="navbar-brand" href="{{ Html::url('/') }}">Mr. Landry's Website</a>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Mobile Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
                    <ul class="nav navbar-nav navbar-right"></ul>
                </nav>
            </div>
        </div>
    </nav>
    <div class="pageContent">
        <!-- <noscript>
            JavaScript is Disabled.
            JavaScript is required to access this website. Please enable JavaScript and refresh the page.
        </noscript> -->
        <div class="container">
            @yield('content')
            <hr>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Forward
                </div>
                <div class="panel-body">
                    This site is built using {{ Html::link('https://bitbucket.org/kalebklein/framework', 'SimpleMVC', array(
                    'target' => '_blank')) }}. The PHP MVC Framework build by me.
                    <br>
                    <br>
                    If you would like to see the SimpleMVC API, {{
                            Html::link('http://api.kalebklein.com/SimpleMVC',
                            'view it here', array(
                            'target' => '_blank')) }}.
                    <br><br>
                    View the application code <a href="https://bitbucket.org/kalebklein/simplemvc" target="_blank">here</a>.
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="footer_text">
                Site &copy; <span class="date"></span>
                {{ Html::link('http://kalebklein.com', 'Kaleb Klein',
                    [], 'true') }}.
                |
                {{ Html::link('http://validator.w3.org/check?uri=referer',
                    'Valid HTML5', [], true) }}
                | SimpleMVC Framework v0.1
            </div>
        </div>
    </div>
</body>
</html>
