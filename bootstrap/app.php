<?php

session_start(); // Make sure sessions are enabled!

require_once __DIR__.'/../vendor/autoload.php';

$app = new \SimpleMVC\Foundation\Application();