<?php

return array(
    // Basic app configuration
    'debug'         => true,

    // Set to true if you use a PHP version that
    // doesn't support the password_* functions
    'crypt_compat'  => false,

    // App Information
	'appname'       => 'landry',
    'basepath'      => '/landry',

    // Where are your routes defined?
    'routes'        => __DIR__.'/../app/routes.php',
    'errors'        => __DIR__.'/../app/errors.php',

    // This framework uses the Blade templating engine
    // Where are your views and where will you cache your
    // views?
    'views'         => __DIR__.'/../app/views/',
    'cache'         => __DIR__.'/../storage/cache/',

    // Application aliases
    // These are used for working with classes in
    // Blade or simple importing of classes in code
    'aliases' => array(
        // Core aliases
        'Config'    => 'SimpleMVC\Utils\ConfigFacade',
        'Session'   => 'SimpleMVC\Session\Session',
        'Auth'      => 'SimpleMVC\Auth\Auth',
        'Html'      => 'SimpleMVC\Html\HtmlBuilder',
        'Form'      => 'SimpleMVC\Html\FormBuilder',
        'Hash'      => 'SimpleMVC\Auth\Hash',
        'KParser'   => 'Pazuzu156\KParser\SimpleMVC\KParserFacade',

        // Place any custom aliases below
    ),
);
