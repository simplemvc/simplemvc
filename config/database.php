<?php

return array(
    // Driver can be 1 of 2 options
    // sqlite or mysql
    'driver'        => 'sqlite',

    // Database user info
    'host'          => 'localhost',
    'username'      => 'root',
    'password'      => '',

    // Database info. Prefix and name
    'name'          => 'landry',
    'prefix'        => 'mvc_',

    // If you use SQLite, you need to point to the database's
    // location
    'sqlite_file'   => __DIR__.'/../storage/database.sqlite',
);
