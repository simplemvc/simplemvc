<?php

use SimpleMVC\Database\Blueprint;
use SimpleMVC\Database\Migration;

class CreateUsersTable extends Migration
{
    /**
     * For pushing migrations up
     *
     * @return void
     */
    public function up()
    {
        $this->create('users', function(Blueprint $table)
        {
            // Place table rows here
            $table->increments('id');
            $table->string('username');
            $table->string('password');
            $table->string('email');
            $table->text('sldkflsdkfj')->after('password');
            $table->integer('user_id')->after('id');
            $table->timestamps();
        });
    }

    /**
     * For reversing migrations
     *
     * @return void
     */
    public function down()
    {
        // This is for removing the table
        $this->drop('users');
    }
}
