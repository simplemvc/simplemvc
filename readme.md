# SimpleMVC
SimpleMVC is a PHP MVC Framework built to be lightweight and simple to use.

## What is this?
It's an MVC (Model, View, Controller) framework built in PHP from scratch. Why build one if there are already others out there? 2 reasons..

1. Dive head-first into building a large-scale application framework  
2. To serve the needs of what I need it for, and change it in any capacity as I see fit.  

## Can I use it now?
Yes/No. It's far from complete. There's still SOOOO many things to implement, and so much is missing, like...the M...No model support yet.

## What's currently implemented?
Basic operating components are implemented. It's not at all a complete MVC framework, as there's still much to do, but here's what it has so far:

1. Controllers - Pages are loaded via controllers
2. Views - You can create pages and layouts using [Laravel's](https://laravel.com/) [Blade Templating Engine](https://laravel.com/docs/5.2/blade) which is built into the framework via [PhiloNL's Standalone Blade Package](https://github.com/PhiloNL/Laravel-Blade)
3. Routing - The router currently only supports GET/POST requests, and doesn't support POST requests very well yet.
4. Rudementary Session Management - Basic session management with NO cookie support yet.

## Installation instructions
You need PHP first and foremost. Since it's a PHP framework, you need it. Make sure you use a PHP version greater than v5.4. You'll also need [Composer](https://getcomposer.org/).

Install the required dependencies with Composer: `$ composer install`

## Required PHP Depencencies
* [pazuzu156/simplemvc](https://bitbucket.org/pazuzu156/simplemvc) - The MVC Framework  
* [pazuzu156/kparser](https://github.com/pazuzu156/kparser) - Unused (for now) The parsing tool used to parse pages from database
* [geshi/geshi](https://github.com/GeSHi/geshi-1.0) - Used by KParser
* [symfony/console](https://github.com/symfony/console) - Used for the CLI

`pazuzu156/mvc-framework` is requested via custom repository defined in composer.json

## CLI
The framework now supports CLI (**C**ommand **L**ine **I**nterface) through Symfony's Console API.

### Current CLI Items
1. `$ php cli db:make create_users_table` - Creates a database migration using create_users_table as the name and class
2. `$ php cli db:push` - Pushes all migrations to the database
3. `$ php cli db:rollback` - Calls each migration's down() method
4. `$ php cli clear-cache` - Clears views cache

### Planned CLI Items
None to list here at the moment. :)

## Contributors
[Kaleb Klein](http://kalebklein.com)

## License
This framework is covered by the [GNU General Public License v2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)
